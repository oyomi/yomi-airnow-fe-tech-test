# Airnow FE Tech Test

## Submission Guideline
Please fork this repository. when you have finished your work please provide a link to your own repository. You are also welcome to host this on Github if you don't have a Bitbucket account. 
## Task	
We would like you to demonstrate, using a technology of your choice, how to fetch and manipulate data in a json format and then present that to a user.

Given the two datasets in this repository, create a Javascript project that creates a component similar to the wire frame provided.

This task should take you somewhere from a few hours to a day. Please don’t take any more time than this, we are more interested in your approach than completing the task.  

Please look at the mockup provided and decide how best to approach this problem.

## Acceptance Criteria
Some basic acceptance criteria is as below:

1. Given I load the component I am presented with a list of installed SDK’s.
2. Given the component is loaded, and ‘Installed’ is selected, when I click the “uninstalled” button, the application will render the list of uninstalled SDK’s.
3. Given the component is loaded, and ‘Uninstalled’ is selected, when I click the “installed” button, the application will render the list of installed SDK’s.
4. Given the list is rendered I would like to see the SDK’s grouped into their categories.
5. Given the component is loaded I would like to see the last seen date for each SDK in a human readable format.
6. Given the component is loaded I would like to see the latest updated date in the format provided in the wire frame.
7. Given the component is loaded I would like to see the total count of SDK’s.

## Outcomes
Some things we would like to see:

- Frequent commits.
- Some testing implementation.
- A choice of application state (context, redux etc)
- Some UI decisions being made, a UI wire frame has been provided but feel free to add your own framework or styles.

It’s up to you what technology you want to use to achieve this. Our current stack is using React and GraphQL but we are more interested in your approach to solving the task given to you. 


<hr />

SOLUTION
========
# Title
A SDK Listing App

# Screenshot


### Features

###### App Features
- Home Page to view all SDKS 
- Sort by "Installed" or "UnInstalled"

### Technology
- HTML
- React Library
- CSS
- Styled Component

### Installation
- Clone the repo in the folder where you want the repo to be saved.
- Navigate into the newly created folder and install the dependencies using your command line: ```cd folder_name && npm install or yarn install```
- Start the app in the development environment by using ```npm run start or yarn run start``` and for production use ```npm run start or yarn run start```
- Make a ```GET``` request to ```http://localhost:3000``` or go to your browser and use the url ```http://localhost:3000```


### Pages
- Home Page: ```/```
- Not Found Page


Estimation
----------
Estimated: 7 hours

Spent: 8 hours

#### How can I build a better "Product"
- Implement a sort on UI (sort in either ascending or descending order on the SDK list)
- Implement a search

### My current Solution
- Implemented the SDKs Library list table
- Implemented a not found page for non-existing routes
- Used CSS-IN-JS approach due to the fact it allows for dynamic css values

### Test Cases:

As user I want to list SDKs

``` gherkin

Feature: SDKs Listing Page:
Given the user visits the website Home Page:

WHEN user visits the SDKs listing page
THEN the user should see the list of Installed SDKs
AND the user should see "10" SDKs in a table

WHEN user clicks the UnInstalled Button Tab on the the SDKs listing page i.e Home page

THEN the user should see the list of Installed SDKs
AND the user should see "6" SDKs in a table

```

### Author
Made with <3 by Yomi Olaoye ;)
