import React from "react";
import styled from "styled-components";

const CustomButton = styled.button`
  background: ${(props) => (!props.active ? "#5586A4" : "#0c3e5d")};
  display: inline-block;
  border-radius:  ${(props) =>
    props.borderSide === "left" ? "50px 0px 0px 50px" : "0px 50px 50px 0px"};
  padding: 10px 0;
  width 150px;
  border: none;
  color: #fff;
  font-size: 12px;
  cursor: pointer;

  &:hover, {
    color: ${(props) => (props.active ? "#5586A4" : "#0c3e5d")};
  }
`;

const Button = ({ title, actionHandler, active = false, borderSide }) => {
  return (
    <CustomButton
      onClick={actionHandler}
      active={active}
      borderSide={borderSide}
    >
      {title}
    </CustomButton>
  );
};

export default Button;
