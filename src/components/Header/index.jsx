import styled from 'styled-components';

const HeaderBox = styled.header`
  height: 80px;
  width: 100%;
  background: #E00038;
  text-align: center;
  color: #fff;
`;

const Header = () => {
  return (
    <HeaderBox>
      <a href='/'>
        <img
          src='https://res.cloudinary.com/kugoo/image/upload/v1645233909/upload/sdk-icon.png'
          alt='logo'
          height='80px'
          width='80px'
        />
      </a>
    </HeaderBox>
  );
};

export default Header;
