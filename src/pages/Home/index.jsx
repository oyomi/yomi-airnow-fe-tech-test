import React, { useState, useEffect } from "react";
import moment from "moment";
import styled from "styled-components";
import Header from "../../components/Header";
import Container from "../../components/Container";

import Button from "../../components/Button";

import { navOptions } from "./data";

// JSON Data Files
import InstalledSDK from "../../data/installed.json";
import UninstalledSDK from "../../data/uninstalled.json";

//helpers
import { titleFormatter } from "../../helpers/titleFormatter";
import { getSdksCategories } from "../../helpers/getSdksCategories";
import { getCategoriesList } from "../../helpers/getCategoriesList";

const HomeBox = styled.div`
  width: 60vw;
  border: 3px solid #5586a4;
  margin: 25px 0px;
`;

const NavOptionBox = styled.div`
  display: flex;
  width: fit-content;
  padding: 10px;
  border: 1px solid #5586a4;
  border-radius: 50px;
`;

const TitleBox = styled.div`
  display: flex;
  margin: 25px 15px 25px 0px;
  color: #000;
`;

const Title = styled.div`
  width: 50%;
  margin: 0px 20px;
`;

const NumberHolder = styled.div`
  width: 50%;
  text-align: right;
`;

const CategoryList = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const CategoryItems = styled.div`
  display: flex;
  margin: 20px 5%;
  flex-direction: column;
  font-size: 16px;
`;

const Home = () => {
  const [selected, setSelected] = useState("");
  const [currentViewData, setCurrentViewData] = useState([]);
  const [total, setTotal] = useState(0);
  const [categories, setCategories] = useState([]);
  const [latestUpdate, setLatestUpdate] = useState(null);

  useEffect(() => {
    setSelected("installed");
  }, []);

  useEffect(() => {
    if (selected === "installed") {
      const sdks = InstalledSDK.data.installedSdks;
      setCurrentViewData(sdks);
      setTotal(sdks?.length);
      setCategories(getSdksCategories(sdks));
      setLatestUpdate(InstalledSDK.data.latestUpdatedDate);
    } else {
      const sdks = UninstalledSDK.data.uninstalledSdks;
      setCurrentViewData(sdks);
      setTotal(sdks?.length);
      setCategories(getSdksCategories(sdks));
      setLatestUpdate(UninstalledSDK.data.latestUpdatedDate);
    }
  }, [selected]);

  const updateButtonState = (selected) => {
    setSelected(selected);
  };

  const listCategoryItems = (category) => {
    return getCategoriesList(currentViewData, category).map((item) => {
      return (
        <div>
          {item.name} {moment(item.lastSeenDate, "YYYYMMDD").fromNow()}
        </div>
      );
    });
  };

  return (
    <>
      <Header />
      <Container>
        <NavOptionBox>
          {navOptions.map((item) => {
            return (
              <Button
                title={item}
                active={item?.toLowerCase() === selected}
                actionHandler={() => updateButtonState(item?.toLowerCase())}
                key={item}
                borderSide={item === "Installed" ? "left" : "right"}
              />
            );
          })}
        </NavOptionBox>
        <HomeBox>
          <TitleBox>
            <Title>{titleFormatter(selected, `SDK's`)}</Title>
            <NumberHolder>{total}</NumberHolder>
          </TitleBox>
          <Title>
            Latest Update: {moment(latestUpdate).format("MMM Do, YYYY")}
          </Title>
          <hr />
          <CategoryList>
            {categories.map((category) => {
              return (
                <CategoryItems>
                  <u>{category}</u>
                  <br />
                  {listCategoryItems(category)}
                </CategoryItems>
              );
            })}
          </CategoryList>
        </HomeBox>
      </Container>
    </>
  );
};

export default Home;
