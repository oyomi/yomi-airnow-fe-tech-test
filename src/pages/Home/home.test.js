/* eslint-disable no-undef */
import { render } from '@testing-library/react';
import Home from './index';

test('renders snapshot', () => {
  render(<Home />);
  const asFragment = render(<Home />).asFragment;
  expect(asFragment()).toMatchSnapshot();
});
