export const getCategoriesList = (data, category) => {
  return data.filter((item) => item?.categories[0] === category);
};
