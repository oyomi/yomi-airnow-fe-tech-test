export const titleFormatter = (text, concatText = "") => {
  return (
    text.charAt(0).toUpperCase() +
    text.substr(1).toLowerCase() +
    " " +
    concatText
  );
};
