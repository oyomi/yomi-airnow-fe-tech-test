export const getSdksCategories = (data) => {
  const categories = [];
  data.forEach((item) => {
    if (!categories.includes(item.categories[0])) {
      categories.push(item.categories[0]);
    }
  });

  return categories;
};
